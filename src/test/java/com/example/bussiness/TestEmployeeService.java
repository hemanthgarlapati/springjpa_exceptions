/**
 * 
 */
package com.example.bussiness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.entity.Employee;
import com.example.repository.EmployeeRepository;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class TestEmployeeService {

	@InjectMocks
	EmployeeService employeeService;

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	Employee employee;

	private static final Logger logger = LoggerFactory.getLogger(TestEmployeeService.class);

	@BeforeAll
	public static void init() {
		
	}

	@BeforeEach
	public void beforeEach() {
		MockitoAnnotations.initMocks(this);
		employee = new Employee(1, "Test1", 20000, 2);
		logger.info("Executes before each test case");
	}

	@Test
	public void testAddEmployee() {
		when(employeeRepository.save(employee)).thenReturn(employee);
		assertEquals(employee, employeeService.addEmployee(employee));
	}

	@Test
	public void testGetEmpById() throws Exception {
		when(employeeRepository.findById(1)).thenReturn(Optional.of(employee));
		assertEquals("Test1", employeeService.findById(1).getName());
	}

	
	/*
	 * @Test public void testExceptionGetEmpById() throws Exception {
	 * 
	 * when(employeeRepository.save(employee)).thenReturn(employee); //
	 * employeeService.addEmployee(employee);
	 * //assertThrows(EmployeeNotFoundException.class, () ->
	 * employeeRepository.findById(101));
	 * 
	 */
	 

	
	  @Test 
	  public void testGetAllEmployees() { 
		  List<Employee> empList=new ArrayList<>();
		  empList.add(employee);
		  empList.add(new Employee(2, "Test2", 20000, 3));
		  when(employeeRepository.findAll()).thenReturn(empList);
		  assertEquals(2, employeeService.getAllEmployees().size());
	  }
	 

	@Test
	public void testDeleteEmployee() {
		employeeService.deleteEmployee(1);
		assertEquals(0, employeeService.getAllEmployees().size());
	}

	@AfterEach
	public void afterEach() {
		logger.info("Executes once after each test case");
	}

	@AfterAll
	public static void destroy() {
	}

}
