package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bussiness.EmployeeService;
import com.example.entity.Employee;
import com.example.exception.EmployeeNotFoundException;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	/**
	 * This is the method for retrieving all the employees from the DB.
	 * 
	 * @param args Unused.
	 * @return This method returns response entity with List of employees.
	 * @exception Exception.
	 */
	@ApiOperation("Retrieve all the employees")
	@GetMapping
	public ResponseEntity<List<Employee>> getAllEmployees() throws Exception {

		List<Employee> employees = employeeService.getAllEmployees();

		if (!employees.isEmpty()) {
			return ResponseEntity.ok().body(employees);
		} else {
			throw new Exception("Employees not found");
		}
	}

	/**
	 * This is the method for retrieving the employee by passing id.
	 * 
	 * @param id.
	 * @return This method returns response entity with employees.
	 * @exception EmployeeNotFoundException.
	 */
	@ApiOperation("Retrieves the EmployeeInfo based on ID")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable Integer id) throws Exception {

		return ResponseEntity.ok().body(employeeService.findById(id));

	}

	/**
	 * This is the method for inserting the employee.
	 * 
	 * @param employee.
	 * @return This method returns response entity with employees.
	 * @exception Nothing.
	 */
	@ApiOperation("Inserts new Employee into DB")
	@PostMapping
	public ResponseEntity<Employee> insertEmployee(@Valid @RequestBody Employee employee) {
		employeeService.addEmployee(employee);
		return ResponseEntity.ok(employeeService.addEmployee(employee));

	}

	/**
	 * This is the method for updating the employee.
	 * 
	 * @param employee.
	 * @param id.
	 * @return This method returns response entity with success message.
	 * @exception Nothing.
	 */
	@ApiOperation("Updates existing Employee")
	@PutMapping(value = "/{id}")
	public ResponseEntity<String> updateEmployee(@Valid @RequestBody Employee employee, @PathVariable Integer id) {

		employeeService.addEmployee(employee);
		return new ResponseEntity<>("Employee with ID: " + employee.getId() + " is updated successfully",
				HttpStatus.OK);

	}

	/**
	 * This is the method for deleting the employee.
	 * 
	 * @param id.
	 * @return This method returns response entity with success message.
	 * @exception Nothing.
	 */
	@ApiOperation("Deletes employee from DB based on ID")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<String> deleteEmployee(@PathVariable Integer id) {

		return new ResponseEntity<>("Employee with ID: " + id + " is deleted successfully", HttpStatus.OK);

	}

	/**
	 * This is the method for retrieving the employees based on experience.
	 * 
	 * @param experience.
	 * @return This method returns response entity with List of employees.
	 * @exception EmployeeNotFoundException.
	 */
	@ApiOperation("Retrieves all the employees with particular experience")
	@GetMapping(value = "/experience/{experience}")
	public ResponseEntity<List<Employee>> getEmpByExperience(@PathVariable Integer experience)
			throws EmployeeNotFoundException {
		return ResponseEntity.ok().body(employeeService.getEmpByExperience(experience));
	}

	/**
	 * This is the method for retrieving the employees based on experience range.
	 * 
	 * @param start.
	 * @param end.
	 * @return This method returns response entity with List of employees.
	 * @exception EmployeeNotFoundException.
	 */
	@ApiOperation("Retrieves employees with  experience in b/w 2 values")
	@GetMapping(value = "/experience/{start}/{end}")
	public ResponseEntity<List<Employee>> findByExperienceBetween(@PathVariable Integer start,
			@PathVariable Integer end) throws EmployeeNotFoundException {
		return ResponseEntity.ok().body(employeeService.findByExperienceBetween(start, end));
	}

}
