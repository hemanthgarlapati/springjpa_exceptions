package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	public List<Employee> findByExperience(Integer experience);
	
	@Query(value="FROM Employee e WHERE e.experience < ?1")
	public List<Employee> findByExperienceLessThan(Integer experience);
	
	@Query(value="FROM Employee e WHERE e.experience > ?1")
	public List<Employee> findByExperienceGreaterThan(Integer experience);
	
	@Query(value="FROM Employee e WHERE e.experience > ?1 and e.experience < ?2")
	public List<Employee> findByExperienceBetween(Integer start, Integer end);
}
