package com.example.bussiness;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.example.entity.Employee;
import com.example.exception.EmployeeNotFoundException;
import com.example.repository.EmployeeRepository;

/**
 * @author hemanth.garlapati
 * 
 */

@Service
public class EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);

	/**
	 * This is the method for retrieving all the employees from the DB.
	 * 
	 * @param args Unused.
	 * @return This method returns List of employees.
	 * @exception Nothing.
	 */
	public List<Employee> getAllEmployees() {
		logger.info("Retrieving all the employees Details");
		return (List<Employee>) employeeRepository.findAll();
	}

	/**
	 * This is the method for deleting the employee from the DB.
	 * 
	 * @param id.
	 * @return This method does not return anything.
	 * @exception Nothing.
	 */

	public void deleteEmployee(Integer id) {
		logger.debug("Deleting the employee with ID: ", id);
		employeeRepository.deleteById(id);

	}

	/**
	 * This is the method for adding a new employee to the DB.
	 * 
	 * @param employee.
	 * @return This method returns employee.
	 * @exception Nothing.
	 */
	public Employee addEmployee(Employee employee) {
		logger.info("Inserting a new  employee");
		return employeeRepository.save(employee);
	}

	/**
	 * This is the method for searching an employee with a particular ID.
	 * 
	 * @param id.
	 * @return This method returns employee.
	 * @exception EmployeeNotFoundException when there is no employee with that
	 *                                      particular ID.
	 */
	public Employee findById(Integer id) throws EmployeeNotFoundException {
		logger.debug("searching an employee with id ", id);
		Optional<Employee> employee = employeeRepository.findById(id);
		if ((ObjectUtils.isEmpty(employee))) {
			throw new EmployeeNotFoundException("No Employee Found with ID:" + id);
		}
		return employee.get();

	}

	/**
	 * This is the method for getting the employee by experience.
	 * 
	 * @param experience.
	 * @return This method returns List of employees.
	 * @exception EmployeeNotFoundException when there is no employee with that
	 *                                      particular experience.
	 */
	public List<Employee> getEmpByExperience(Integer experience) throws EmployeeNotFoundException {

		List<Employee> employeeList = employeeRepository.findByExperience(experience);
		logger.info("retrieving all the employees with an experience of ", experience);
		if (!(ObjectUtils.isEmpty(employeeList)))
			return employeeRepository.findByExperience(experience);
		else
			throw new EmployeeNotFoundException("No Employee Found with " + experience + " year of Experience");
	}

	/**
	 * This is the method for finding the employees with in range of experience.
	 * 
	 * @param start.
	 * @param end.
	 * @return This method returns employee.
	 * @exception EmployeeNotFoundException when there is no employee with that
	 *                                      particular ID.
	 */

	public List<Employee> findByExperienceBetween(Integer start, Integer end) throws EmployeeNotFoundException {
		logger.info("searching for employee within  a range");
		List<Employee> employeeList = employeeRepository.findByExperienceBetween(start, end);
		if (!(ObjectUtils.isEmpty(employeeList)))
			return employeeRepository.findByExperienceBetween(start, end);
		else
			throw new EmployeeNotFoundException("No Employee Found with that range Experience");

	}

}
