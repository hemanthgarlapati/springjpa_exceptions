package com.example.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeExpDTO {
	
	private Integer experience;
	private Long count;

}
