/**
 * 
 */
package com.example.exception;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
@AllArgsConstructor
public class CustomErrorResponse {
	private String message;
	private List<String> details;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	LocalDateTime timestamp;
}
